Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  ## API ##
  namespace :api, defaults: { format: "json" } do
  	namespace :v1 do
  		get 'movies', to: 'movies#index'
  		post 'movies', to: 'movies#create'
  		get 'bookings', to: 'bookings#index'
  		post 'bookings', to: 'bookings#create'
  	end
  end
end
