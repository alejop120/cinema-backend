class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :email, null: false
      t.integer :document, null: false, limit: 8, unique: true
      t.string :name, null: false
      t.integer :cell_phone, null: false, limit: 8

      t.timestamps
    end
  end
end
