class CreateBookings < ActiveRecord::Migration[6.0]
  def change
    create_table :bookings do |t|
      t.date :date, null: false
      t.references :movie_theater, null: false
      t.references :movie, null: false
      t.references :user, null: false

      t.timestamps
    end
  end
end
