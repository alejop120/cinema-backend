class CreatePresentationDays < ActiveRecord::Migration[6.0]
  def change
    create_table :presentation_days do |t|
      t.date :date, null: false
      t.references :movie, null: false, foreign_key: true, index: true

      t.timestamps
    end
  end
end
