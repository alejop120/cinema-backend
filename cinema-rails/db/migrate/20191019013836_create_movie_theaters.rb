class CreateMovieTheaters < ActiveRecord::Migration[6.0]
  def change
    create_table :movie_theaters do |t|
      t.string :name, null: false
      t.integer :chairs, null: false

      t.timestamps
    end
  end
end
