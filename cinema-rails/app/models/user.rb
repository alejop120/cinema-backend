class User < ApplicationRecord

	validates :email, presence: true
	validates :document, presence: true
	validates :name, presence: true
	validates :cell_phone, presence: true
end
