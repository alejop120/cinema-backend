class Movie < ApplicationRecord
	has_many :presentation_days, dependent: :destroy
	has_many :bookings, dependent: :destroy

	validates :presentation_days, presence: true
	validates :name, presence: true
	validates :synopsis, presence: true
	validates :image_url, presence: true

	accepts_nested_attributes_for :presentation_days
end
