class Booking < ApplicationRecord
	belongs_to :movie_theater
	belongs_to :movie
	belongs_to :user

	validates :date, presence: true

	accepts_nested_attributes_for :user

	def as_json(options={})
		super(include: [:movie, :user])
	end
end
