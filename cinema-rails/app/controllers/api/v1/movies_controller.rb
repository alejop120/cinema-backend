module Api::V1
	class MoviesController < ApplicationController

		def index
			result = { success: false, message: "Películas." }
			json_schema = "app/controllers/api/v1/schemas/movies/index.json"
			begin
				JSON::Validator.validate!(json_schema, params.to_json)

				result[:movies] = Movie.where(id: PresentationDay.all.where(date: params[:date]).pluck(:movie_id))

			rescue JSON::Schema::ValidationError => e
				result[:message] = e.message
			end
			render json: result
		end

		def create
			result = { success: false, message: "Película creada correctamente." }
			json_schema = "app/controllers/api/v1/schemas/movies/create.json"
			begin
				JSON::Validator.validate!(json_schema, params.to_json)

				new_movie = Movie.new(movie_params)
				if new_movie.save
					result[:success] = true
				else
					result[:message] = "Error when creating movie: #{new_movie.errors.full_messages}"	
				end
			rescue JSON::Schema::ValidationError => e
				result[:message] = e.message
			end
			render json: result
		end

		private

			def movie_params
				params.require(:movie).permit(:name, :synopsis, :image_url, presentation_days_attributes: [:date])
			end
	end
end