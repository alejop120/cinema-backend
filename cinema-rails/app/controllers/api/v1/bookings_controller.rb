module Api::V1
	class BookingsController < ApplicationController

		def index
			result = { success: true, message: "Reservas" }
			result[:bookings] = Booking.all
			render json: result
		end

		def create
			result = { success: false, message: "Reserva realizada correctamente." }
			json_schema = "app/controllers/api/v1/schemas/bookings/create.json"
			begin
				JSON::Validator.validate!(json_schema, params.to_json)

				movie = Movie.find_by_id(booking_params[:movie_id])
				if movie.present?
					limit_chairs = 10
					if movie.bookings.count < limit_chairs
						new_booking = Booking.new(booking_params)
						if new_booking.save
							result[:success] = true
						else
							result[:message] = "Error when creating booking: #{new_booking.errors.full_messages}"	
						end
					else
						result[:message] = "La película cuenta con el límite de reservas [#{limit_chairs}]"
					end
				else
					result[:message] = "Película no éxiste"
				end
			rescue JSON::Schema::ValidationError => e
				result[:message] = e.message
			end
			render json: result
		end

		private

			def booking_params
				params.require(:booking).permit(:date, :movie_theater_id, :movie_id, user_attributes: [:email, :document, :name, :cell_phone])
			end
	end
end